#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>


#include <opencv2/objdetect.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>

#include <iostream>
#include <sstream>
#include <string>
#include <ctime>
#include <cstdio>
#include <vector>
#include <math.h>


using namespace std;
using namespace cv;
using namespace xfeatures2d;


int main()
{
	string file1, file2;
	Mat image1, image2, image1ROI;
	Rect objectRect;

	cout << "Enter the name of the first picture." << endl;
	cin >> file1;
	cout << "Enter the name of the second picture." << endl;
	cin >> file2;

	image1 = imread("../"+ file1, IMREAD_GRAYSCALE);
	image2 = imread("../" + file2, IMREAD_GRAYSCALE);

	imshow("First picture", image1);
	imshow("Second picture", image2);


	objectRect = selectROI("First picture", image1);	//drag select
	
	image1ROI = image1(objectRect);
	imshow("Object picture", image1ROI);

	// Detect keypoints and compute descriptors

	int featureNum = 400;	//more accurate with default feature number
	Mat objectDescriptors, sceneDescriptors;
	Ptr<SIFT> detector = SIFT::create();
	std::vector<KeyPoint> objectKeypoints;
	detector->detectAndCompute(image1ROI, noArray(),objectKeypoints, objectDescriptors);

	Mat imageObjectKeypoints;
	drawKeypoints(image1ROI, objectKeypoints, imageObjectKeypoints);
	imshow("Object keypoints", imageObjectKeypoints);

	std::vector<KeyPoint> sceneKeypoints;
	detector->detectAndCompute(image2, noArray(), sceneKeypoints, sceneDescriptors);

	Mat imageSceneKeypoints;
	drawKeypoints(image2, sceneKeypoints, imageSceneKeypoints);
	imshow("Secene keypoints", imageSceneKeypoints);

	//Match the descritpors

	Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create(DescriptorMatcher::FLANNBASED);
	vector<vector<DMatch>> knnMatches;
	matcher->knnMatch(objectDescriptors, sceneDescriptors, knnMatches, 2);
	
	const float ratio_thresh = 0.75f;
	vector<DMatch> goodMatches;

	for (size_t i = 0; i < knnMatches.size(); i++) {
		if (knnMatches[i][0].distance < ratio_thresh * knnMatches[i][1].distance) {
			goodMatches.push_back(knnMatches[i][0]);
		}
	}

	Mat imageMatches;
	drawMatches(image1ROI, objectKeypoints, image2, sceneKeypoints, goodMatches, imageMatches, Scalar::all(-1), Scalar::all(-1), vector<char>(),
		DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);


	vector<Point2f> object, scene;
	for (size_t i = 0; i < goodMatches.size(); i++) {
		object.push_back(objectKeypoints[goodMatches[i].queryIdx].pt);
		scene.push_back(sceneKeypoints[goodMatches[i].trainIdx].pt);
	}

	Mat H;
	try {
		H = findHomography(object, scene, RANSAC);
	}
	catch (Exception e) {
		cout << "Can't find homography matrix..." << endl;
		return -1;
	}
	vector<Point2f> object_corners(4);
	object_corners[0] = Point2f(0.0, 0.0);
	object_corners[1] = Point2f(float(image1ROI.cols), 0.0);
	object_corners[2] = Point2f(float(image1ROI.cols), float(image1ROI.rows));
	object_corners[3] = Point2f(0.0, float(image1ROI.rows));

	vector<Point2f> scene_corners(4);
	try {
		perspectiveTransform(object_corners, scene_corners, H);

		line(imageMatches, scene_corners[0] + object_corners[1],
			scene_corners[1] + object_corners[1], Scalar(255, 0, 0), 2);
		line(imageMatches, scene_corners[1] + object_corners[1],
			scene_corners[2] + object_corners[1], Scalar(255, 0, 0), 2);
		line(imageMatches, scene_corners[2] + object_corners[1],
			scene_corners[3] + object_corners[1], Scalar(255, 0, 0), 2);
		line(imageMatches, scene_corners[3] + object_corners[1],
			scene_corners[0] + object_corners[1], Scalar(255, 0, 0), 2);

	}
	catch (Exception e) {
		cout << "\nCouldn't find a match..." << endl;
		imageMatches = Mat::zeros(Size(600, 400), CV_64FC1);
		putText(imageMatches, "Couldn't find a match", Point(120, 200),
			FONT_HERSHEY_SIMPLEX, 1, Scalar(255, 255, 255), 1);
	}

	imshow("Object and scene matches", imageMatches);



	waitKey(0);


	return 0;
}
