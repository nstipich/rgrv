#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>


#include <opencv2/objdetect.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>

#include <iostream>
#include <sstream>
#include <string>
#include <ctime>
#include <cstdio>
#include <vector>
#include <math.h>


using namespace std;
using namespace cv;
using namespace xfeatures2d;

void Convert2DPointsTo3DPoints(vector<KeyPoint>& points2D_L, vector<KeyPoint>& points2D_R, Mat& E, Mat& P, Mat& points3D)
{

	//Find the inverse of P (projection matrix)
	Mat Pinv;

	invert(P, Pinv, DECOMP_SVD);

	//Determine the singular value decomposition (svd) of E (essential matrix)
	Mat U, V, Vt, D;
	SVD::compute(E, D, U, Vt);

	//Define W
	Mat W = Mat::zeros(3, 3, CV_32F);
	W.at<float>(0, 1) = -1;
	W.at<float>(1, 0) = 1;
	W.at<float>(2, 2) = 1;

	Mat A = U * W * Vt;

	Mat b = Mat::zeros(3, 1, CV_32F);
	b.at<float>(0, 0) = U.at<float>(0, 2);
	b.at<float>(1, 0) = U.at<float>(1, 2);
	b.at<float>(2, 0) = U.at<float>(2, 2);

	Mat Ainv, Ainv_b;
	invert(A, Ainv, DECOMP_SVD);
	Ainv_b = Ainv * b;

	/**** Helper matrices ****/
	Mat Lpi = Mat(3, 1, CV_32F);
	Mat Rpi = Mat(3, 1, CV_32F);
	Mat ARpi = Mat(3, 1, CV_32F);

	Mat S = Mat(2, 1, CV_32F);

	Mat X = Mat(2, 2, CV_32F);
	Mat x1 = Mat(1, 1, CV_32F);
	Mat x2 = Mat(1, 1, CV_32F);
	Mat x4 = Mat(1, 1, CV_32F);

	Mat Y = Mat(2, 1, CV_32F);
	Mat y1 = Mat(1, 1, CV_32F);
	Mat y2 = Mat(1, 1, CV_32F);

	//2D points in left (model) and right (scene) images
	Mat Lm = Mat(3, 1, CV_32F);
	Mat Rm = Mat(3, 1, CV_32F);

	//Iteratively convert 2D point pairs to 3D points
	for (size_t i = 0; i < points2D_L.size(); i++)
	{
		Lm.at<float>(0, 0) = points2D_L[i].pt.x;
		Lm.at<float>(1, 0) = points2D_L[i].pt.y;
		Lm.at<float>(2, 0) = 1;

		Rm.at<float>(0, 0) = points2D_R[i].pt.x;
		Rm.at<float>(1, 0) = points2D_R[i].pt.y;
		Rm.at<float>(2, 0) = 1;

		Lpi = Pinv * Lm;
		Rpi = Pinv * Rm;

		//Init X table
		ARpi = Ainv * Rpi;
		x1 = Lpi.t() * Lpi;
		x2 = Lpi.t() * ARpi;
		x4 = ARpi.t() * ARpi;

		X.at<float>(0, 0) = -x1.at<float>(0, 0);
		X.at<float>(0, 1) = x2.at<float>(0, 0);
		X.at<float>(1, 0) = x2.at<float>(0, 0);
		X.at<float>(1, 1) = -x4.at<float>(0, 0);

		//Init Y table
		y1 = Lpi.t() * Ainv_b;
		y2 = ARpi.t() * Ainv_b;

		Y.at<float>(0, 0) = -y1.at<float>(0, 0);
		Y.at<float>(1, 0) = y2.at<float>(0, 0);

		solve(X, Y, S);

		float s = S.at<float>(0, 0);
		float t = S.at<float>(1, 0);

		Lpi.at<float>(0, 0) = s * Lpi.at<float>(0, 0);
		Lpi.at<float>(1, 0) = s * Lpi.at<float>(1, 0);
		Lpi.at<float>(2, 0) = s * Lpi.at<float>(2, 0);

		ARpi.at<float>(0, 0) = t * ARpi.at<float>(0, 0);
		ARpi.at<float>(1, 0) = t * ARpi.at<float>(1, 0);
		ARpi.at<float>(2, 0) = t * ARpi.at<float>(2, 0);

		points3D.at<float>(0, i) = (Lpi.at<float>(0, 0) + ARpi.at<float>(0, 0) - Ainv_b.at<float>(0, 0)) / 2;
		points3D.at<float>(1, i) = (Lpi.at<float>(1, 0) + ARpi.at<float>(1, 0) - Ainv_b.at<float>(1, 0)) / 2;
		points3D.at<float>(2, i) = (Lpi.at<float>(2, 0) + ARpi.at<float>(2, 0) - Ainv_b.at<float>(2, 0)) / 2;
	}
}