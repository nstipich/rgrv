#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>


#include <opencv2/objdetect.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>

#include <iostream>
#include <sstream>
#include <string>
#include <ctime>
#include <cstdio>
#include <vector>
#include <math.h>


using namespace std;
using namespace cv;
using namespace xfeatures2d;

void Convert2DPointsTo3DPoints(vector<KeyPoint>& points2D_L, vector<KeyPoint>& points2D_R, Mat& E, Mat& P, Mat& points3D);
vector<Mat> Calibrate(int value);
void ransac3Planes(vector<Point3f>* points3Dv);
FILE* pts3Dfile;

int main() {
	string file1, file2, pointsFile;
	Mat image1, image2, cameraMatrix, distCoeffs;
	vector<Mat> calibrationValues;

	int calibrate;
	pointsFile = "points3D.txt";

	cout << "Calibrate or not? [0/1]" << endl;
	cin >> calibrate;
	if (calibrate == 0) {
		calibrationValues = Calibrate(0);
	}
	else if (calibrate == 1) {
		calibrationValues = Calibrate(1);
	}
	cameraMatrix = calibrationValues[0];
	distCoeffs = calibrationValues[1];

	cout << "Enter the name of the first picture." << endl;
	cin >> file1;
	cout << "Enter the name of the second picture." << endl;
	cin >> file2;

	image1 = imread(file1, IMREAD_GRAYSCALE);
	image2 = imread(file2, IMREAD_GRAYSCALE);



	imshow("First picture", image1);
	imshow("Second picture", image2);
	waitKey(0);


	//Keypoints & Descriptors

	int featureNum = 400;	//more accurate with default feature number
	Mat image1Descriptors, image2Descriptors;
	Ptr<SIFT> detector = SIFT::create();

	//1st image
	std::vector<KeyPoint> image1Keypoints;
	detector->detectAndCompute(image1, noArray(), image1Keypoints, image1Descriptors);

	Mat image1MatKeypoints;
	drawKeypoints(image1, image1Keypoints, image1MatKeypoints);
	imshow("Frist image keypoints", image1MatKeypoints);

	//2nd image
	std::vector<KeyPoint> image2Keypoints;
	detector->detectAndCompute(image2, noArray(), image2Keypoints, image2Descriptors);

	Mat image2MatKeypoints;
	drawKeypoints(image2, image2Keypoints, image2MatKeypoints);
	imshow("Second image keypoints", image2MatKeypoints);
	waitKey(0);


	//Match the descritpors
	Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create(DescriptorMatcher::FLANNBASED);
	vector<vector<DMatch>> knnMatches;
	matcher->knnMatch(image1Descriptors, image2Descriptors, knnMatches, 2);

	const float ratio_thresh = 0.75f;
	vector<DMatch> goodMatches;

	for (size_t i = 0; i < knnMatches.size(); i++) {
		if (knnMatches[i][0].distance < ratio_thresh * knnMatches[i][1].distance) {
			goodMatches.push_back(knnMatches[i][0]);
		}
	}

	Mat imageMatches;
	drawMatches(image1, image1Keypoints, image2, image2Keypoints, goodMatches, imageMatches, Scalar::all(-1), Scalar::all(-1), vector<char>(),
		DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
	imshow("Image matches", imageMatches);
	waitKey(0);

	vector<Point2f> image1Pts, image2Pts;
	for (size_t i = 0; i < goodMatches.size(); i++) {
		image1Pts.push_back(image1Keypoints[goodMatches[i].queryIdx].pt);
		image2Pts.push_back(image2Keypoints[goodMatches[i].trainIdx].pt);
	}
	Mat F, mask;
	F = findFundamentalMat(image1Pts, image2Pts, FM_RANSAC, 3., 0.99, mask);

	vector<uchar> arrayMask;
	if (mask.isContinuous()) {
		arrayMask.assign(mask.data, mask.data + mask.total());
	}
	else {
		for (int i = 0; i < mask.rows; i++) {
			arrayMask.insert(arrayMask.end(), mask.ptr<uchar>(i),
				mask.ptr<uchar>(i) + mask.cols);
		}
	}

	Mat combined_image;

	hconcat(image1, image2, combined_image);

	std::vector<KeyPoint> image1Kpts, image2Kpts;
	RNG rng(12488);
	for (int i = 0; i < arrayMask.size(); i++) {
		if (arrayMask[i] == 1) {
			image1Kpts.push_back(KeyPoint(image1Pts[i], 1.f));
			image2Kpts.push_back(KeyPoint(image2Pts[i], 1.f));


			Scalar color = Scalar(rng.uniform(0, 255));

			circle(combined_image, image1Pts[i], 3, color, 1, CV_AA);
			circle(combined_image, image2Pts[i] + Point2f(image1.cols, 0),
				3, color, 1, CV_AA);

			line(combined_image, image1Pts[i],
				image2Pts[i] + Point2f(image1.cols, 0), color,
				1, CV_AA, 0);
		}
	}
	imshow("Combined image", combined_image);
	waitKey(0);

	F.convertTo(F, CV_32F);
	cameraMatrix.convertTo(cameraMatrix, CV_32F);

	Mat E;
	try {
		E = cameraMatrix.t() * F * cameraMatrix;
	}
	catch (exception e) {
		cout << "Couldn't calculate essential matrix." << endl;
		return -1;
	}

	Mat points3D = Mat(3, image1Kpts.size(), CV_32F);
	Convert2DPointsTo3DPoints(image1Kpts, image2Kpts, E, cameraMatrix, points3D);

	fopen_s(&pts3Dfile, pointsFile.c_str(), "w");
	vector<Point3f> points3Dv;

	for (int i = 0; i < points3D.cols; i++) {
		fprintf(pts3Dfile, "%f %f %f \n", points3D.at<float>(0, i),
			points3D.at<float>(1, i), points3D.at<float>(2, i));

		points3Dv.push_back(Point3f(points3D.at<float>(0, i), points3D.at<float>(1, i),
			points3D.at<float>(2, i)));
	}
	fclose(pts3Dfile);
	return 0;
}

