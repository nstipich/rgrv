// RG_LV1.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"

#include <conio.h>
#include <vector>
#include <math.h>

#include <iostream>
#include <sstream>
#include <string>
#include <ctime>
#include <cstdio>


//#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>





using namespace cv;
using namespace std;

static void onMouse(int event, int x, int y, int flags, void* param);

struct UserInput {
	int nClicks = 0;
	Mat image_points = Mat::zeros(4, 2, CV_32F);
	Mat object_points = Mat::zeros(4, 3, CV_32F);
};

int main(int argc, char *argv[])
{
	Mat cameraMatrix, distCoeffs;
	VideoCapture cap(0, CAP_DSHOW); // cap(0) -> open the default/inbuilt camera
	
	int calibrationStep;
	cout << "Press 0 to calibrate camera or 1 to take already calibrated params: " << endl;
	cin >> calibrationStep;



	if (calibrationStep == 0) {


	int m_MaxNImages;	//max number of images taken  (5)
	int m_BoardWidth;	//board width (ie the max number of corners that can be found along the chessboard length) (8)
	int m_BoardHeight;	//board height(ie the max number of corners that can be found along the chessboard height) (6)
	float m_SquareLength;	//the length of one of the squares on the chessboard (in mm)  (36)


	cout << "Max number of images to take:";
	cin >> m_MaxNImages;
	cout << "Board width (ie the max number of corners that can be found along the chessboard width):";
	cin >> m_BoardWidth;
	cout << "Board height(ie the max number of corners that can be found along the chessboard height):";
	cin >> m_BoardHeight;
	cout << "Length of one of the squares on the chessboard (in mm):";
	cin >> m_SquareLength;

	Size imageSize;

	vector<Point2f> corners;
	vector<vector<Point2f> > imagePoints;

	Mat cameraMatrix, distCoeffs;

	Size board_sz = Size(m_BoardWidth, m_BoardHeight);


	int successes = 0;
	const int ESC_KEY = 27;




	if (cap.isOpened())  // check if we succeeded
	{

		int c = 0;
		bool found;
		namedWindow("Current view", 1);
		for (;;)
		{
			c = waitKey(15);

			// get a new frame from camera
			Mat frame, imgclone, imggray;

			cap >> frame;
			//display the frame 
			imshow("Current view", frame);

			imageSize = frame.size();
			//Use frame to determine chessboard corners:
			if (c == 'p')
			{
				if (successes < m_MaxNImages)
				{
					imgclone = frame.clone();
					imggray.create(imgclone.size(), CV_8UC1);

					//Find chessboard corners:
					found = findChessboardCorners(imgclone, board_sz, corners, CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE);

					if (found)
					{
						// improve the found corners' coordinate accuracy for chessboard
						Mat viewGray;
						cvtColor(imgclone, imggray, COLOR_BGR2GRAY);
						cornerSubPix(imggray, corners, Size(11, 11),
							Size(-1, -1), TermCriteria(TermCriteria::EPS + TermCriteria::COUNT, 30, 0.1));

						// Draw the corners.
						drawChessboardCorners(imgclone, board_sz, Mat(corners), found);

						imagePoints.push_back(corners);

						successes++;
					}
					imshow("Calibration", imgclone);
				}
				else
					break;
			}

			if (c == ESC_KEY) break;
		}
	}
	// the camera will be deinitialized automatically in VideoCapture destructor
	//END COLLECTION WHILE LOOP.

	//close all windows
	destroyAllWindows();



	if (successes == m_MaxNImages)
	{
		//PERFORM CALIBRATION
		vector<Mat> rvecs, tvecs;
		vector<float> reprojErrs;
		double totalAvgErr = 0;

		cameraMatrix = Mat::eye(3, 3, CV_64F);
		distCoeffs = Mat::zeros(8, 1, CV_64F);

		vector<vector<Point3f> > objectPoints(1);

		//Compute board_corners
		for (int i = 0; i < board_sz.height; ++i)
			for (int j = 0; j < board_sz.width; ++j)
				objectPoints[0].push_back(Point3f(j*m_SquareLength, i*m_SquareLength, 0));


		objectPoints.resize(imagePoints.size(), objectPoints[0]);

		//Find intrinsic and extrinsic camera parameters
		double rms;
		rms = calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs, 0);

		cout << "Re-projection error reported by calibrateCamera: " << rms << endl;

		bool ok = checkRange(cameraMatrix) && checkRange(distCoeffs);

		//Calculate average reprojection error
		//totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints, rvecs, tvecs, cameraMatrix, distCoeffs, reprojErrs);

		cout << (ok ? "Calibration succeeded" : "Calibration failed");



		//SAVE PARAMS
		FileStorage fs("cameraparams.xml", FileStorage::WRITE);
		fs << "camera_matrix" << cameraMatrix;
		fs << "distortion_coefficients" << distCoeffs;
		fs.release();



		// LOAD THESE MATRICES BACK IN AND DISPLAY THE UNDISTORTED IMAGE
		Mat cameraMatrixLoaded, distCoeffsLoaded;
		FileStorage fsL("cameraparams.xml", FileStorage::READ);
		fsL["camera_matrix"] >> cameraMatrixLoaded;
		fsL["distortion_coefficients"] >> distCoeffsLoaded;
		fsL.release();


		if (cap.isOpened())  // get frame from camera
		{
			int c = 0;
			
			namedWindow("Original view", 1);
			for (;;)
			{
				c = waitKey(15);

				// get a new frame from camera
				Mat frame, imgclone, imgUndistort;

				cap >> frame;

				//display the frame 
				imshow("Original view", frame);

				imgclone = frame.clone();

				//Undistort
				undistort(imgclone, imgUndistort, cameraMatrix, distCoeffs);

				imshow("Undistorted view", imgUndistort);

				if (c == ESC_KEY) break;
			}
		}


		//close all windows
		destroyAllWindows();
	}
		}

	else if (calibrationStep == 1) {
		cameraMatrix = Mat::eye(3, 3, CV_64F);
		distCoeffs = Mat::zeros(8, 1, CV_64F);
		FileStorage fs("cameraparams.xml", FileStorage::READ);
		fs["camera_matrix"] >> cameraMatrix;
		fs["distortion_coefficients"] >> distCoeffs;
		fs.release();
	}

	else {
		cout << "Only binary number!" << endl;
}

	//Line detection and calculation
	Mat image;
	int houghStep;
	cout << "Enter 0 to capture a new image or 1 to load an existing one: " << endl;
	cin >> houghStep;

	if (houghStep == 0) {
		if (cap.isOpened())  
		{
			int c = 0;
			namedWindow("Capture", 1);
			for (;;)
			{
				c = waitKey(15);

				Mat frame, imgclone, imgUndistort;

				cap >> frame;

				imshow("Capture", frame);

				Mat img_tmp;
				cap >> img_tmp;
				imwrite("../image.jpg", img_tmp); //save capture to file
				cvtColor(img_tmp, image, CV_RGB2GRAY);

				if (c == 'p') break;
			}
		}
	}
	else if (houghStep == 1) {
		image = imread("../image.jpg", IMREAD_GRAYSCALE); //read from file
	}
	else {
		cout << "Only binary input" << endl;
		return -1;
	}

	namedWindow("Image loaded");
	imshow("Image loaded", image);


	UserInput user_input;

	//points for ROI
	user_input.object_points.at<float>(0, 0) = 0.0f;
	user_input.object_points.at<float>(0, 1) = 0.0f;
	user_input.object_points.at<float>(0, 2) = 0.0f;

	user_input.object_points.at<float>(1, 0) = 250.0f;
	user_input.object_points.at<float>(1, 1) = 0.0f;
	user_input.object_points.at<float>(1, 2) = 0.0f;

	user_input.object_points.at<float>(2, 0) = 0.0f;
	user_input.object_points.at<float>(2, 1) = 173.0f;
	user_input.object_points.at<float>(2, 2) = 0.0f;

	user_input.object_points.at<float>(3, 0) = 250.0f;
	user_input.object_points.at<float>(3, 1) = 173.0f;
	user_input.object_points.at<float>(3, 2) = 0.0f;

	setMouseCallback("Image loaded", onMouse, &user_input);

	
	for (;;) {
		int c = waitKey(15);
		if (user_input.nClicks == 4) {
			setMouseCallback("Image loaded", NULL);	//callback set to NULL after 4 selected points
			break;
		}
	}

	//Define ROI rectangle sizes and position
	float cropped_width = user_input.image_points.at<float>(1, 0) - user_input.image_points.at<float>(0, 0);
	float cropped_height = user_input.image_points.at<float>(2, 1) - user_input.image_points.at<float>(0, 1);
	float u0 = user_input.image_points.at<float>(0, 0);
	float v0 = user_input.image_points.at<float>(0, 1);


	Rect roiRect(u0, v0, cropped_width, cropped_height);
	Mat imageROI = image(roiRect);
	imshow("ROI Display", imageROI);	//displaying of ROI
	cvtColor(imageROI, imageROI, COLOR_GRAY2BGR);




	Mat imageCanny, color_imageCanny;
	Canny(imageROI, imageCanny, 40, 100, 3);
	cvtColor(imageCanny, color_imageCanny, COLOR_GRAY2BGR);
	imshow("Canny Display", imageCanny);	//displaying of Canny

	vector<Vec2f> lines;
	HoughLines(imageCanny, lines, 1, CV_PI / 180, 50, 0, 0);

	if (lines.size() > 0) {
		//Draw the most dominant line
		float rho = lines[0][0];
		float theta = lines[0][1];

		float a = cos(theta);
		float b = sin(theta);

		float x0 = a * rho;
		float y0 = b * rho;

		Point pt1, pt2;
		pt1.x = cvRound(x0 + 1000 * (-b));
		pt1.y = cvRound(y0 + 1000 * (a));
		pt2.x = cvRound(x0 - 1000 * (-b));
		pt2.y = cvRound(y0 - 1000 * (a));
		line(color_imageCanny, pt1, pt2, Scalar(0, 0, 255), 3, LINE_AA);
		line(imageROI, pt1, pt2, Scalar(0, 0, 255), 3, LINE_AA);

		imshow("Color Canny Display", color_imageCanny);

		rho = rho + u0 * cos(theta) + v0 * sin(theta);

		//Rotation and translation vectors
		Mat rotationV, translationV;
		solvePnP(user_input.object_points, user_input.image_points,
			cameraMatrix, distCoeffs, rotationV, translationV);

		//Rotation matrix from vector
		Mat rotationM;
		Rodrigues(rotationV, rotationM);


		Mat A;
		gemm(cameraMatrix, rotationM, 1, 0, 0, A);


		Mat bb;
		gemm(cameraMatrix, translationV, 1, 0, 0, bb);


		float lambda_x = A.at<double>(0, 0) * cos(theta) +
			A.at<double>(1, 0) * sin(theta) - rho * A.at<double>(2, 0);
		float lambda_y = A.at<double>(0, 1) * cos(theta) +
			A.at<double>(1, 1) * sin(theta) - rho * A.at<double>(2, 1);
		float lambda_rho = bb.at<double>(2, 0) * rho -
			bb.at<double>(0, 0) * cos(theta) - bb.at<double>(1, 0) * sin(theta);

		float thetaN = atan2(lambda_y, lambda_x);
		float rhoN = lambda_rho / sqrt(lambda_x * lambda_x + lambda_y * lambda_y);

		string theta_on_img = "thetaN: " + to_string(thetaN * 180 / CV_PI);
		string rho_on_img = "rhoN: " + to_string(rhoN) + "mm";

		putText(imageROI, theta_on_img, Point(imageROI.cols - 300, imageROI.rows - 50),
			FONT_HERSHEY_SIMPLEX, 0.5, Scalar(255, 0, 0), 1, LINE_AA);
		putText(imageROI, rho_on_img, Point(imageROI.cols - 300, imageROI.rows - 30),
			FONT_HERSHEY_SIMPLEX, 0.5, Scalar(255, 0, 0), 1, LINE_AA);

		imshow("ROI Display", imageROI);
	}
	else {
		cout << "No lines are detected." << endl;
		return -1;
	}
	waitKey();
	return 0;
}


static void onMouse(int event, int x, int y, int flags, void* param) {
	if (event == CV_EVENT_LBUTTONDOWN) {
		UserInput* user_input = (UserInput*)param;

		user_input->image_points.at<float>(user_input->nClicks, 0) = x;	//connects image and object points
		user_input->image_points.at<float>(user_input->nClicks, 1) = y;

		user_input->nClicks++;
	}
}



