// LV2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <random>

#define WINDOW_WIDTH 440
#define WINDOW_HEIGHT 440

#define DRAW_WIDTH 400
#define DRAW_HEIGHT 400

#define OFFSET 20

using namespace cv;
using namespace std;

Mat window;
Mat window_copy;



bool isInTriangle(Point T, int edgeId, int nextEdge, Subdiv2D* subdiv) {
    Point2f X1, X2, X3;
    subdiv->edgeOrg(edgeId, &X1);
    subdiv->edgeDst(edgeId, &X2);
    subdiv->edgeDst(nextEdge, &X3);

    bool x1InTriangle = X1.x >= OFFSET && X1.x <= WINDOW_WIDTH - OFFSET && X1.y >= OFFSET && X1.y <= WINDOW_HEIGHT - OFFSET;
    bool x2InTriangle = X2.x >= OFFSET && X2.x <= WINDOW_WIDTH - OFFSET && X2.y >= OFFSET && X2.y <= WINDOW_HEIGHT - OFFSET;
    bool x3InTriangle = X3.x >= OFFSET && X3.x <= WINDOW_WIDTH - OFFSET && X3.y >= OFFSET && X3.y <= WINDOW_HEIGHT - OFFSET;
    bool t1InTriangle = X3.x >= OFFSET && X3.x <= WINDOW_WIDTH - OFFSET && X3.y >= OFFSET && X3.y <= WINDOW_HEIGHT - OFFSET;

    if (t1InTriangle) {
        if (x1InTriangle && x2InTriangle && x3InTriangle) {
            float mi = ((X2.x - X1.x) * (T.y - X1.y) - (X2.y - X1.y) * (T.x - X1.x)) / ((X3.y - X1.y) * (X2.x - X1.x) - (X2.y - X1.y) * (X3.x - X1.x));
            float lambda = (T.x - X1.x - mi * (X3.x - X1.x)) / (X2.x - X1.x);
            if (lambda >= 0.0 && mi >= 0.0 && (lambda + mi) <= 1.0) {
                return true;
            }
        }
    }
    return false;
}

void paintNeighbourTriangles(int firstEdgeId, int secondEdgeId, int thirdEdgeId, Subdiv2D* subdiv) {
    Point2f firstEdgeNextLeft, secondEdgeNextLeft, thirdEdgeNextLeft;

    subdiv->edgeDst(subdiv->getEdge(firstEdgeId, Subdiv2D::NEXT_AROUND_LEFT), &firstEdgeNextLeft);
    subdiv->edgeDst(subdiv->getEdge(secondEdgeId, Subdiv2D::NEXT_AROUND_LEFT), &secondEdgeNextLeft);
    subdiv->edgeDst(subdiv->getEdge(thirdEdgeId, Subdiv2D::NEXT_AROUND_LEFT), &thirdEdgeNextLeft);

    if (firstEdgeNextLeft.x >= OFFSET && firstEdgeNextLeft.x <= WINDOW_WIDTH - OFFSET && firstEdgeNextLeft.y >= OFFSET && firstEdgeNextLeft.y <= WINDOW_WIDTH - OFFSET) {
        Point2f X1, X2;
        subdiv->edgeOrg(firstEdgeId, &X1);
        subdiv->edgeDst(firstEdgeId, &X2);
        vector<Point> points;
        points.push_back(X1);
        points.push_back(X2);
        points.push_back(firstEdgeNextLeft);
        fillConvexPoly(window_copy, points, Scalar(255, 0, 0), CV_AA, 0);
    }

    if (secondEdgeNextLeft.x >= OFFSET && secondEdgeNextLeft.x <= WINDOW_WIDTH - OFFSET && secondEdgeNextLeft.y >= OFFSET && secondEdgeNextLeft.y <= WINDOW_WIDTH - OFFSET) {
        Point2f X1, X2;
        subdiv->edgeOrg(secondEdgeId, &X1);
        subdiv->edgeDst(secondEdgeId, &X2);
        vector<Point> points;
        points.push_back(X1);
        points.push_back(X2);
        points.push_back(secondEdgeNextLeft);
        fillConvexPoly(window_copy, points, Scalar(255, 0, 0), CV_AA, 0);
    }

    if (thirdEdgeNextLeft.x >= OFFSET && thirdEdgeNextLeft.x <= WINDOW_WIDTH - OFFSET && thirdEdgeNextLeft.y >= OFFSET && thirdEdgeNextLeft.y <= WINDOW_WIDTH - OFFSET) {
        Point2f X1, X2;
        subdiv->edgeOrg(thirdEdgeId, &X1);
        subdiv->edgeDst(thirdEdgeId, &X2);
        vector<Point> points;
        points.push_back(X1);
        points.push_back(X2);
        points.push_back(thirdEdgeNextLeft);
        fillConvexPoly(window_copy, points, Scalar(255, 0, 0), CV_AA, 0);
    }
}

void paintHomeTriangle(Point2f first, Point2f second, Point2f third) {
    vector<Point> points;
    points.push_back(first);
    points.push_back(second);
    points.push_back(third);

    fillConvexPoly(window_copy, points, Scalar(0, 0, 255), CV_AA, 0);
}

void onMouse(int event, int x, int y, int flags, void* korisnickiPodaci) {
    if (event == CV_EVENT_LBUTTONDOWN) {
        Point clicked(x, y);
        Subdiv2D* parameters = (Subdiv2D*)korisnickiPodaci;

        if (clicked.x >= OFFSET && clicked.x <= (DRAW_WIDTH + OFFSET) && clicked.y >= OFFSET && clicked.y <= (OFFSET + DRAW_HEIGHT)) {

            int nearest_pt = parameters->findNearest(clicked);
            int first_edge;

            parameters->getVertex(nearest_pt, &first_edge);

            int temp_edge = first_edge;
            while (true) {
                int next_edge = parameters->getEdge(temp_edge, Subdiv2D::NEXT_AROUND_ORG);
                if (isInTriangle(clicked, temp_edge, next_edge, parameters)) {
                    Point2f X1, X2, X3;
                    parameters->edgeOrg(temp_edge, &X1);
                    parameters->edgeDst(temp_edge, &X2);
                    parameters->edgeDst(next_edge, &X3);

                    window_copy = window.clone();

                    paintNeighbourTriangles(parameters->rotateEdge(temp_edge, 2), next_edge, parameters->rotateEdge(parameters->getEdge(temp_edge, Subdiv2D::NEXT_AROUND_LEFT), 2), parameters);
                    paintHomeTriangle(X1, X2, X3);

                    imshow("Delaunay triangulation", window_copy);

                    break;
                }
                else {
                    temp_edge = next_edge;
                    if (temp_edge == first_edge) {
                        break;
                    }
                }
            }
        }


    }


}

int main()
{
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(20, 420);

    window.create(WINDOW_WIDTH, WINDOW_HEIGHT, CV_8UC3);
    window.setTo(255);
    //Mat points(DRAW_HEIGHT, DRAW_WIDTH, CV_8UC3);

    Rect mainWindow(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
    Rect drawWindow(OFFSET, OFFSET, DRAW_WIDTH, DRAW_HEIGHT);

    Subdiv2D draw(mainWindow);
//    Point2f point(distr(gen), distr(gen));

    draw.insert(Point2f(OFFSET, OFFSET));
    draw.insert(Point2f(OFFSET , (OFFSET+DRAW_HEIGHT)));
    draw.insert(Point2f(OFFSET + DRAW_WIDTH, (OFFSET+DRAW_HEIGHT)));
    draw.insert(Point2f((OFFSET + DRAW_WIDTH), OFFSET));


    for (int i = 0; i < 16; i++) {
        Point2f point(distr(gen), distr(gen));
        draw.insert(point);
    }

    vector<int> edges;

   for (int i = 4; i < 24; i++) {
        int first_edge;
        draw.getVertex(i, &first_edge);

        edges.push_back(first_edge);

        int temp_edge = first_edge;
        while (true) {
            int next_edge = draw.getEdge(temp_edge, Subdiv2D::NEXT_AROUND_ORG);
            if (first_edge == next_edge) {
                break;
            }
            else {
                edges.push_back(next_edge);
                temp_edge = next_edge;
            }
            cout << (i - 3) << ". " << draw.getVertex(i) << endl;
        }
        }
   cout << "Num of edges in list: " << edges.size() << endl;

   for (auto item = edges.begin(); item != edges.end(); ++item) {

       Point2f pt_dst, pt_org;

       if (draw.edgeDst(*item, &pt_dst) > draw.edgeOrg(*item, &pt_org)) {
           line(window, pt_org, pt_dst, CV_RGB(0, 0, 0));
       }
   }

   namedWindow("Delaunay triangulation");
   imshow("Delaunay triangulation", window);

   setMouseCallback("Delaunay triangulation", onMouse, (void*)(&draw));
   waitKey(0);
   return 0;
}

